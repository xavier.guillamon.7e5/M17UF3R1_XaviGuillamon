using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalScript : MonoBehaviour
{
    private GameObject[] exitsGameObjects = new GameObject[2];
    private void Start()
    {
        GameManager.Instance.PortalAppear += AppearPortal;
        for (int i = 0; i < transform.childCount; i++)
            exitsGameObjects[i] = transform.GetChild(i).gameObject;
    }

    public void AppearPortal()
    {
        StartCoroutine(Waiter());
    }
    IEnumerator Waiter()
    {
        //Wait for 4 seconds
        yield return new WaitForSeconds(1);
        exitsGameObjects[0].SetActive(true);

        yield return new WaitForSeconds(1);
        exitsGameObjects[1].SetActive(true);

    }
}
