using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderAttackEnemy : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent<IDamageable>(out IDamageable bear))
        {
            bear.OnHurt(1);
        }
    }
}
