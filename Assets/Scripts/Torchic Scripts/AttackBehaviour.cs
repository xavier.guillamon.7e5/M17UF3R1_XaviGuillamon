using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackBehaviour : MonoBehaviour
{
    [SerializeField] private GameObject colliderAttack;

    public void Attack()
    {
        colliderAttack.SetActive(true);
    }

    public void NoAttack()
    {
        colliderAttack.SetActive(true);
    }
}
