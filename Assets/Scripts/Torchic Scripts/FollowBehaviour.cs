using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FollowBehaviour : PokemonMovement, IDamageable
{
    public float hp = 30f;
    private NavMeshAgent _agent;
    [SerializeField] Transform follow;
    [SerializeField] LayerMask obstacleMask, playerMask;

    [SerializeField] float newRadious, newAngle;
    [SerializeField] float timeUntilForget;
    [SerializeField] float attackDistance;
    
    private float _forget;
    private FieldOfView _fov;
    private float _originalRadius, _originalAngle;
    private EnemyStates _enemyStates;

    public void Follow()
    {
        _agent.destination = follow.position;
        if (Vector3.Distance(transform.position, follow.position) < 20)
        {
            Attack();
        }
        
    }

    public void Update()
    {
        Follow();
        _fov.FoV();
        Forget();
    }
    
    void UpdateFovValues()
    {
        _fov.visionAngle = newAngle;
        _fov.visionRadius = newRadious;
    }

    void RestoreFovValues()
    {
        _fov.visionAngle = _originalAngle;
        _fov.visionRadius = _originalRadius;
    }

    private void OnEnable()
    {
        if (follow == null) follow = GameObject.Find("Zincraw").GetComponent<Transform>();
        
        _fov = GetComponent<FieldOfView>();
        _agent = GetComponent<NavMeshAgent>();
        _agent.speed = 8;

        UpdateFovValues();

        _originalRadius = _fov.visionRadius;
        _originalAngle = _fov.visionAngle;

        _forget = 0;

        _enemyStates = GetComponent<EnemyStates>();
    }

    private void OnDisable()
    {
        RestoreFovValues();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, newRadious);
    }

    private void Forget()
    {
        Collider[] ObjesctsInsideTheRadius = Physics.OverlapSphere(transform.position, newRadious, playerMask);

        if (ObjesctsInsideTheRadius.Length != 0)
        {
            Transform target = ObjesctsInsideTheRadius[0].transform;
            Vector3 targetDirection = (target.position - transform.position).normalized;

            if (Vector3.Angle(transform.forward, targetDirection) < newAngle / 2)
            {
                float distanceToTarget = Vector3.Distance(transform.position, target.position);

                if (!Physics.Raycast(transform.position, targetDirection, distanceToTarget, obstacleMask)) _forget = 0;
                else _forget += Time.deltaTime;
            }
            else _forget += Time.deltaTime;
        }
        else _forget += Time.deltaTime;

        if (_forget >= timeUntilForget) _enemyStates.ChangeState(EnemyStates.EnemyState.Patrol);      
    }

    public void Attack()
    {
        anim.SetTrigger("Attack");
    }

    public void OnHurt(float damage)
    {
        Debug.Log(":( " + hp);
        hp -= damage;
        if (hp <= 0)
        {
            Destroy(this.gameObject);
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bullet"))
        {
            OnHurt(3);
        }
    }
}
