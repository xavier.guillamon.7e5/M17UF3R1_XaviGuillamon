using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Serialization;

public class PatrolBehaviour : PokemonMovement
{
    [SerializeField] Transform[] _patrolRoutePoints;
    [SerializeField] int _currentPoint;
    [SerializeField] GameObject _patrolPoints;
    
    Vector3 _target;
    NavMeshAgent _agent;

    private void OnEnable()
    {
        _agent = GetComponent<NavMeshAgent>();
        GetCloserPatrolPoint();
        UpdateDestination();
    }
    public void Update()
    {
        anim.SetBool("Move", true);
        if (Vector3.Distance(transform.position, _target) < 1.25f)
        {
            anim.SetBool("Move", false);
            ChangeDestinationIndex();
            UpdateDestination();
        }
    }
    void UpdateDestination()
    {
        _target = _patrolRoutePoints[_currentPoint].position;
        _agent.SetDestination(_target);
    }
    
    void ChangeDestinationIndex()
    {
        _currentPoint++;
        if (_currentPoint == _patrolRoutePoints.Length) _currentPoint = 0;
    }

    void GetCloserPatrolPoint()
    {
        var allPoints = _patrolPoints.GetComponentsInChildren<Transform>();

        allPoints = allPoints.Skip(1).ToArray();

        var closerPoint = allPoints[0];

        foreach (var point in allPoints) 
            if (Vector3.Distance(transform.position, point.transform.position)
                < Vector3.Distance(transform.position, closerPoint.transform.position))
            {
                closerPoint = point;
            }

        _target = closerPoint.transform.position;

        for (int i = 0; i < allPoints.Length; i++)
        {
            if (allPoints[i] == closerPoint)
            {
                _currentPoint = i;
                break;
            }
        }
    }
}
