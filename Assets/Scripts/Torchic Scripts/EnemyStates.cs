using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStates : MonoBehaviour
{
    [SerializeField] PatrolBehaviour _patrol;
    [SerializeField] FollowBehaviour _follow;
    [SerializeField] AttackBehaviour _attack;
    
    public EnemyState _enemyStates;
    public enum EnemyState
    {
        Patrol,
        Follow,
        Attack
    }

    private void Awake()
    {
        _enemyStates = EnemyState.Patrol;
    }

    public void ChangeState(EnemyState state)
    {
        _enemyStates = state;
        
        DisableBehaviours();

        switch (_enemyStates)
        {
            case EnemyState.Patrol:
                _patrol.enabled = true;
                break;
            case EnemyState.Follow:
                _follow.enabled = true;
                break;
            case EnemyState.Attack:
                _attack.enabled = true;
                break;
        }
    }

    private void DisableBehaviours()
    {
        _patrol.enabled = false;
        _follow.enabled = false;
    }
}
