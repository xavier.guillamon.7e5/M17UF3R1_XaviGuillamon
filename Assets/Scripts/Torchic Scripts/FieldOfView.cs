using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfView : MonoBehaviour
{
    public float visionRadius, visionAngle;
    [SerializeField] private LayerMask playerMask, obstacleMask;

    public GameObject playerToFollow;
    public bool canSeePlayer;

    EnemyStates enemyStates;

    private void Start()
    {
        enemyStates = GetComponent<EnemyStates>();
    }

    void Update()
    {
        FoV();
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, visionRadius);
    }

    public void FoV()
    {
        Collider[] ObjesctsInsideTheRadius = Physics.OverlapSphere(transform.position, visionRadius, playerMask);

        if (ObjesctsInsideTheRadius.Length != 0)
        {
            Transform target = ObjesctsInsideTheRadius[0].transform;

            Vector3 targetDirection = (target.position - transform.position).normalized;

            if (Vector3.Angle(transform.forward, targetDirection) < visionAngle / 2)
            {
                float distanceToTarget = Vector3.Distance(transform.position, target.position);

                if (!Physics.Raycast(transform.position, targetDirection, distanceToTarget, obstacleMask))
                {
                    canSeePlayer = true;
                    enemyStates.ChangeState(EnemyStates.EnemyState.Follow);
                }
                else canSeePlayer = false;
            }
            else
                canSeePlayer = false;
        }
        else if (canSeePlayer) canSeePlayer = false;
    }
}
