using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

public class UIManager : MonoBehaviour
{
    [SerializeField] public HelpOverScreen helpOverScreen;
    [SerializeField] public SettingsScreen settingsScreen;
    public void LoadGame()
    {
        SceneManager.LoadScene("GameScene");
    }
    public void SettingsButton()
    {
        Instantiate(settingsScreen);
        settingsScreen.Setup();
    }
    public void LoadMenu()
    {
        SceneManager.LoadScene("MainScene");
    }
    public void HelpButton()
    {
        Instantiate(helpOverScreen);
        helpOverScreen.Setup();
    }

}
