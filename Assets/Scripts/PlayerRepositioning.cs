using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRepositioning : MonoBehaviour
{
    
    void Start()
    {
        StartCoroutine(SetCharacterPosition()); // A�adir que sea solo si el personaje se mueve
    }

    IEnumerator SetCharacterPosition()
    {
        yield return new WaitForSeconds(0.01f);
        transform.position = transform.parent.position - 1f * Vector3.up;
        transform.rotation = transform.parent.rotation;
        yield return SetCharacterPosition();
    }
}
