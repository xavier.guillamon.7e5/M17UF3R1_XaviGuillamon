using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GettingItems : MonoBehaviour, ICollectable
{
    [SerializeField] private Inventory _inventory;
    [SerializeField] private ItemData[] _item;

    public void GetItem(string name)
    {
        for(int i = 0; i < _item.Length; i++)
        {
            if(_item[i].Name == name)
            {
                _inventory.Item.Add(_item[i]);
            }
        }
    }
}
