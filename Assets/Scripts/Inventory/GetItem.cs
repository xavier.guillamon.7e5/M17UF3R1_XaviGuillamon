using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GetItem : MonoBehaviour
{
    [SerializeField] private ItemData _item;
    [SerializeField] private GameObject _itemEquiped;

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.TryGetComponent<ICollectable>(out ICollectable item))
        {
            GameManager.Instance.IncreaseItemEquipped();
            Debug.Log(GameManager.Instance.GetItemsCollected());
            Destroy(gameObject.GetComponent<Collider>());
            _itemEquiped.SetActive(true);
            item.GetItem(_item.Name);
            Destroy(gameObject);
        }
    }

}
