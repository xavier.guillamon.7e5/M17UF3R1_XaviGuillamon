using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Inventory Data", menuName = "Inventory Data")]
public class Inventory : ScriptableObject
{
    public List<ItemData> Item;

    public void OutInventory()
    {
        Item.Clear();
    }
}
