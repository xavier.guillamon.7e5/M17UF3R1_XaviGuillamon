using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item Data", menuName = "Item Data")]
public class ItemData : ScriptableObject
{
    [SerializeField] private string _name;
    [SerializeField] private GameObject _item;

    public string Name { get { return _name; } }
    public GameObject Item { get { return _item;  } }
}
