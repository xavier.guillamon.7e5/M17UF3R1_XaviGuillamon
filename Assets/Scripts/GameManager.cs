using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    private int itemsCollected;
    public event Action PortalAppear = delegate { };
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("GameManager is NULL");
            }
            return _instance;
        }
    }
    private void Awake()
    {
        _instance = this;
    }
    public void IncreaseItemEquipped()
    {
        itemsCollected++;
        CheckItemsCollected();
    }
    public int GetItemsCollected()
    {
        return itemsCollected;
    }
    public void CheckItemsCollected()
    {
        if(itemsCollected > 2)
        {
            PortalAppear.Invoke();
        }
    }
}
