using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ThirdPersonMovement : MonoBehaviour, IDamageable
{
    // HP
    public float hp = 3f;
    bool isInvulnerable = false;
    float invulnerabilityTime = 2f;

    // Camera and controller
    public CharacterController controller;
    public Transform cam;
    private Animator anim;

    // Bullet
    public GameObject bulletPrefab;
    public Transform bulletSpawnPoint;
    public Transform bulletDirection;
    public float bullets = 35;
    private float bulletSpeed = 25f;
    public float shootCooldown;
    private readonly float shootCooldownShootTime = 0.1f;
    public float currentBullets;

    // Movement
    public float camDistance = 3f;
    public float camDistanceWhileAiming = 1f;
    public float speed = 3f;
    public float runSpeed = 6f;
    public float swimmingSpeed = 2f;
    public bool isSwimming;
    public Transform camAiming;

    // Jump
    public float jumpHeight = 2f;
    public float jumpHeightWhileRunning = 3f;
    public float gravity = -20f;

    // Smooth movement when rotate
    public float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;

    // Vectors movement
    private Vector3 moveDirection = Vector3.zero;
    Vector3 velocity;

    void Start()
    {
        anim = GameObject.Find("Player").GetComponent<Animator>();
        currentBullets = bullets;
        shootCooldown = shootCooldownShootTime;
    }

    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        shootCooldown -= Time.deltaTime;
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;
        anim.SetFloat("Speed", 0f);
        anim.SetBool("IsSwimming", isSwimming);
        Debug.DrawLine(transform.position, transform.position - 1.5f * Vector3.down, Color.red, 0.01f);
        if (Physics.Raycast(transform.position, Vector3.down, out RaycastHit hit, 1.5f))
        {
            if (hit.collider.gameObject.tag == "Water")
            {
                isSwimming = true;
                speed = swimmingSpeed;
            }
            else
            {
                isSwimming = false;
                speed = 3f;
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && controller.isGrounded && !isSwimming)
        {
            if (Input.GetKey(KeyCode.LeftShift))
                velocity.y = Mathf.Sqrt(jumpHeightWhileRunning * -2f * gravity);
            else
                velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }

        if (Input.GetMouseButton(0) && currentBullets != 0)
        {
            if (direction.magnitude <= 0.1f) anim.SetBool("IsShootingQuiet", true);
            if (shootCooldown < 0)
            {
                Shooting();
                shootCooldown = shootCooldownShootTime;
            }
        }
        else anim.SetBool("IsShootingQuiet", false);

        if (Input.GetKeyDown(KeyCode.R))
        {
            RechargeBullets();
        }

        if (controller.isGrounded) anim.SetBool("IsGrounded", true);
        else anim.SetBool("IsGrounded", false);

        if (direction.magnitude >= 0.1f)
        {
            anim.SetFloat("Speed", 0.5f);
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity,
                turnSmoothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            if (Input.GetKey(KeyCode.LeftShift) && !isSwimming)
            {
                speed = runSpeed;
                anim.SetFloat("Speed", 1f);
            }
            else
                speed = 3f;

            moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            controller.Move(moveDirection.normalized * speed * Time.deltaTime);
        }

        if (Input.GetMouseButton(1)) Debug.Log("Im working"); //camDistance = camDistanceWhileAiming;
        else camDistance = 3f;

        if (direction == Vector3.zero) speed = 0f;
        velocity.y += gravity * Time.deltaTime;
        controller.Move((moveDirection.normalized * speed + velocity) * Time.deltaTime);
    }

    void LateUpdate()
    {
        Vector3 offset = new Vector3(0f, 1.5f, -camDistance);
        cam.position = transform.position + offset;
        cam.LookAt(transform.position);
    }

    public void Shooting()
    {
        var shoot = Instantiate(bulletPrefab, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
        Vector3 direction = bulletDirection.TransformDirection(Vector3.forward);
        shoot.GetComponent<Rigidbody>().velocity = direction * bulletSpeed;
        currentBullets--;
    }

    public void RechargeBullets()
    {
        currentBullets = 35;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Portal"))
        {
            SceneManager.LoadScene("GameScene2");
        }
        if (other.gameObject.CompareTag("Final"))
        {
            SceneManager.LoadScene("WinScene");
        }
        if (other.gameObject.CompareTag("Damage") && isInvulnerable)
        {
            OnHurt(1);
            isInvulnerable = true;
            Invoke("ResetInvulnerability", invulnerabilityTime);
        }
    }
    public void OnHurt(float damage)
    {
        hp -= damage;
        if (hp <= 0)
        {
            SceneManager.LoadScene("LoseScene");
        } 
    }
    void ResetInvulnerability()
    {
        isInvulnerable = false;
    }
}